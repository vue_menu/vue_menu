import { createApp } from 'vue'
import App from './App.vue'
import Home from './Home.vue'
import About from './About.vue'
import MenuList from './MenuList.vue'
import Add from './Add.vue'
import SignUp from './SignUp.vue'
import Login from './Login.vue'

import axios from 'axios'

import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    { path: '/', component: Home },
    { path: '/menu-list', component: MenuList },
    { path: '/add', component: Add },
    { path: '/about', component: About },
    { path: '/SignUp', component: SignUp },
    { path: '/Login', component: Login }



]

// Creo il router e passo le rotte definite su routes
const router = createRouter({
    history: createWebHistory(),
    routes: routes,
})

const app = createApp(App);
// dico all'applicazione di usare il router
app.use(router, axios);
app.mount('#app');